FROM        ubuntu:18.04

LABEL       author="Trimerus" maintainer="trimerus@hsfactory.net"

# Install Dependencies
RUN         dpkg --add-architecture i386
RUN         apt-get update && apt-get upgrade -y


RUN         apt-get install -y --install-recommends \
                                software-properties-common \
                                curl \
                                gcc \
                                g++ \
                                apt-utils \
                                lib32gcc1 \
                                libntlm0 \
                                libgcc1 \
                                libcurl4-gnutls-dev:i386 \
                                libssl1.0.0:i386 \
                                lib32tinfo5 \
                                libtinfo5:i386 \
                                lib32z1 \
                                lib32stdc++6 \
                                libncurses5:i386 \
                                libcurl3-gnutls:i386 \
                                iproute2 gdb \
                                libsdl1.2debian \
                                libfontconfig \
                                telnet \
                                net-tools\
                                netcat \
                                htop \
                                tar \
                                wget \
                                gnupg \
                                wine64 \ 
                                wine32 \
                                wine-gecko \
                                wine-mono

RUN		    wget -q -O /usr/sbin/winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks \
		    && chmod +x /usr/sbin/winetricks
		    && wget -q -O /wine/gecko_x86.msi http://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86.msi \
		    && wget -q -O /wine/gecko_x86_64.msi http://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86_64.msi \
		    && wget -q -O /wine/mono.msi http://dl.winehq.org/wine/wine-mono/4.9.3/wine-mono-4.9.3.msi \
		    && chmod -R 0444 /wine

ENV		    DISPLAY=:0
ENV		    DISPLAY_WIDTH=1024
ENV 	    DISPLAY_HEIGHT=768
ENV		    DISPLAY_DEPTH=16
ENV		    AUTO_UPDATE=1
ENV		    XVFB=1
ENV		    WINEPREFIX=/home/container/.wine

# Add wine repository
#RUN         wget -nc https://dl.winehq.org/wine-builds/winehq.key
#RUN         apt-key add winehq.key
#RUN         wget -qO- https://dl.winehq.org/wine-builds/Release.key | apt-key add -
#RUN         apt-get -y install --install-recommends software-properties-common \
#            && add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main' \
#            && apt-get update

# RUN        apt-get -y install --install-recommends winehq-stable=4.0.4~bionic cabextract

RUN         useradd -m -d /home/container container

USER        container
ENV         HOME /home/container
WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh
CMD 		["/bin/sh", "/entrypoint.sh"]

