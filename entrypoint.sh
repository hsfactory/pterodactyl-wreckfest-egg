#!/bin/bash

cd /home/container
sleep 1
# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`

# Download SteamCMD, it is missing
if [ ! -f "/home/container/steamcmd/steamcmd.sh" ]; then

    if [ -d "/home/container/steamapps" ]; then
        rm -rf /home/container/steamapps
    fi

    if [ ! -d "steamcmd" ]; then
        mkdir steamcmd
        cd steamcmd
    fi

    if  curl -sSL -o steamcmd.tar.gz "http://media.steampowered.com/installer/steamcmd_linux.tar.gz"; then
        tar -xzvf steamcmd.tar.gz
        rm -rf steamcmd.tar.gz
    else
        echo "There was an error while attempting to download SteamCMD (exit code: $?)"
        exit 404
    fi

    echo "Installing requested game, this could take a long time depending on game size and network."
    echo -e "./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir /home/container +app_update ${SRCDS_APPID} +quit" >runscript_update
    
    set -x
    ./steamcmd.sh +@sSteamCmdForcePlatformType windows +force_install_dir /home/container +login anonymous +app_update ${SRCDS_APPID} +quit
    set +x
    
    cd /home/container
    echo "Creating .steam/sdk32 directory and copying steamclient.so"
    mkdir -p .steam/sdk32
    cp -v steamcmd/linux32/steamclient.so .steam/sdk32/steamclient.so

    mkdir .wine
    echo "Installing Gecko"
    wine msiexec /i /wine/gecko_x86.msi /qn /quiet /norestart /log .wine/gecko_x86_install.log
    wine msiexec /i /wine/gecko_x86_64.msi /qn /quiet /norestart /log .wine/gecko_x86_64_inatall.log

    echo "Installing mono"
    wine msiexec /i /wine/mono.msi /qn /quiet /norestart /log .wine/mono_install.log
else
    echo "Dependencies in place, to re-download this game please delete steamcmd.sh in /steamcmd/steamcmd.sh."
fi

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval "$MODIFIED_STARTUP"

